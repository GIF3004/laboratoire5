#!/bin/bash
set -e

# Sync executable
bn=$(basename $1)
rpiaddr_serveur="inserer_adresse_de_votre_raspberry_pi_serveur_ici"
rpiaddr_client="inserer_adresse_de_votre_raspberry_pi_client_ici"

if [ "$2" == "all" ]; then
    rsync -az $1/build/serveur "pi@$rpiaddr_serveur:/home/pi/projects/laboratoire5/"
    rsync -az $1/build/client "pi@$rpiaddr_client:/home/pi/projects/laboratoire5/"
else

    if [ "$2" == "client" ]; then
        rpiaddr=$rpiaddr_client
    else
        rpiaddr=$rpiaddr_serveur
    fi
    rsync -az $1/build/$2 "pi@$rpiaddr:/home/pi/projects/laboratoire5/"
    # Execute GDB
    ssh "pi@$rpiaddr" "nohup gdbserver :$3 /home/pi/projects/laboratoire5/$2 --debug > /dev/null 2> /dev/null < /dev/null &"
    sleep 1
fi
